/* BEGIN LICENSE
  * Copyright © Blue Mind SAS, 2012-2024
  *
  * This file is part of Blue Mind. Blue Mind is a messaging and collaborative
  * solution.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of either the GNU Affero General Public License as
  * published by the Free Software Foundation (version 3 of the License)
  * or the CeCILL as published by CeCILL.info (version 2 of the License).
  *
  * There are special exceptions to the terms and conditions of the
  * licenses as they are applied to this program. See LICENSE.txt in
  * the directory of this program distribution.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  *
  * See LICENSE.txt
  * END LICENSE
  */
package net.bluemind.user.service.internal;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Verticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import net.bluemind.directory.service.DirEventProducer;
import net.bluemind.lib.vertx.IUniqueVerticleFactory;
import net.bluemind.lib.vertx.IVerticleFactory;
import net.bluemind.mailbox.api.MailboxBusAddresses;

public class UserChangedVerticle extends AbstractVerticle {
	public static class Factory implements IVerticleFactory, IUniqueVerticleFactory {
		@Override
		public boolean isWorker() {
			return true;
		}

		@Override
		public Verticle newInstance() {
			return new UserChangedVerticle();
		}
	}

	@Override
	public void start() {
		EventBus eb = getVertx().eventBus();
		eb.consumer(DirEventProducer.address, (Message<JsonObject> event) -> {
			String uid = event.body().getString("uid");
			UserService.getCache().invalidate(uid);
		});
		eb.consumer(MailboxBusAddresses.UPDATED, (Message<JsonObject> event) -> {
			String uid = event.body().getString("itemUid");
			UserService.getCache().invalidate(uid);
		});
		eb.consumer(MailboxBusAddresses.DELETED, (Message<JsonObject> event) -> {
			String uid = event.body().getString("itemUid");
			UserService.getCache().invalidate(uid);
		});
	}
}
